package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ThreadControllerTests {
    private Thread thr;
    private String slug = "slug_naroda";
    private String id = "123894567";

    @BeforeEach
    @DisplayName("Testing the thread")
    void crtThrTst() {
        thr = new Thread("potok", new Timestamp(0), "forum_room", "Sprashivayu", slug, "bye world", 1488);
    }

    @Test
    @DisplayName("ID and Slug Checker")
    void chkIdSlugTst() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt(id))).thenReturn(thr);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
            assertEquals(thr, controller.CheckIdOrSlug(id), "Id found");
            assertEquals(thr, controller.CheckIdOrSlug(slug), "Slug found.");

        }
    }

    @Test
    @DisplayName("Post creater")
    void crtPstTst() {
        List<Post> poststhr = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(poststhr), controller.createPost(slug, poststhr));
        }
    }

    @Test
    @DisplayName("Post getter")
    void pstTst() {
        List<Post> poststhr = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getPosts(thr.getId(), 200, 2, null, false)).thenReturn(poststhr);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(poststhr), controller.Posts(slug, 200, 2, null, false));
        }
    }

    @Test
    @DisplayName("Thread changer")
    void cngTst() {
        Thread changeThread = new Thread("ona_persona", new Timestamp(2), "forum_room", "a mozhno sprosit", "slug_lug", "HTO YA", 121212);
        changeThread.setId(2);
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug_lug")).thenReturn(changeThread);
            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.change("slug_lug", thr), "Change succesfull");
        }
    }

    @Test
    @DisplayName("Thread infrmation")
    void infTst() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.info(slug), "Information succesfull");
        }
    }

    @Test
    @DisplayName("Vote creater")
    void crtVtTst() {
        Vote vote = new Vote("bibika", 5);
        User user = new User("bibika", "za@lupa.com", "Don Bidon", "UU AA");
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try(MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                ThreadController controller = new ThreadController();

                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
                userMock.when(() -> UserDAO.Info("bibika")).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.createVote(slug, vote), "Successfull");
            }
        }
    }
}
